module.exports = function(app) {

    var listaProdutos = function(req, res) {
        var connection = app.infra.connectionFactory();
        var produtosDAO = new app.infra.ProdutosDAO(connection);

        produtosDAO.lista(function(err, resultados) {
          res.format({
            html: function(){
              res.render('produtos/lista', {lista:resultados});
            },
            json: function () {
              res.json(resultados);
            }
          });

        });

        connection.end();
    }

    app.get('/produtos', listaProdutos);

    app.get('/produtos/form', function(req, res) {
        res.render('produtos/form', {errosValidacao : {}, produto:{}});
    });

    app.post('/produtos', function(req, res) {

          var produto = req.body;
          req.assert('titulo','Título é obrigatório').notEmpty();
          req.assert('preco','Formato inválido').isFloat();

          var erros = req.validationErrors();
          console.log(erros);
          if(erros){
              res.render('produtos/form', {errosValidacao : erros, produto:produto});
              return;
          }


        var connection = app.infra.connectionFactory();
        var produtosDAO = new app.infra.ProdutosDAO(connection);

        produtosDAO.salva(produto, function(err, results) {
            res.redirect('/produtos');
        });

        connection.end();
    });

    app.get('/produtos/json',function(req,res){
        var connection = app.infra.connectionFactory();
        var produtosDAO = new app.infra.ProdutosDAO(connection);
        produtosDAO.lista(function(erros, resultados){
            res.json(resultados);
        });
        connection.end();
    });
}
